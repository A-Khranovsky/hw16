<?php
require_once ('../db.php');
/** @var PDO $pdo */
$sql = "delete from tb_references where id = :id;";

$result = $pdo->prepare($sql);
$result->bindParam(':id', $_GET['id']);
$result->execute();

header("Location: /admin/references/");
