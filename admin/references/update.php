<?php
/** @var PDO $pdo */
$errorBag = [
    'testimonial' => [],
    'person' => [],
    'position' => [],
    'image' => []
];
$data = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $testimonial = $_POST['Testimonial'];
    $person = $_POST['Person'];
    $position = $_POST['Position'];
    $image = $_POST['Image'];

    if (empty($testimonial)) {
        $errorBag['testimonial'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($testimonial)) {
            $errorBag['testimonial'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($testimonial) < 5) {
                $errorBag['testimonial'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($testimonial) > 255) {
                $errorBag['testimonial'][] = 'Значение более 255 символов';
            }
        }
    }

    if (empty($person)) {
        $errorBag['person'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($person)) {
            $errorBag['person'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($person) < 5) {
                $errorBag['person'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($person) > 255) {
                $errorBag['person'][] = 'Значение более 255 символов';
            }
        }
    }

    if (empty($position)) {
        $errorBag['position'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($position)) {
            $errorBag['position'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($position) < 5) {
                $errorBag['position'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($position) > 255) {
                $errorBag['position'][] = 'Значение более 255 символов';
            }
        }
    }

    if (empty($image)) {
        $errorBag['image'][] = 'Поле не должно быть пустым';
    } else {
        if (filter_var($image, FILTER_VALIDATE_URL) === false) {
            $errorBag['image'][] = 'Нужно указать URL';
        }
    }
    $errorsCounter = count($errorBag['testimonial'] + $errorBag['image'] + $errorBag['person'] + $errorBag['position']);
    if ($errorsCounter == 0) {
        require_once ('../db.php');
        $sql = "update tb_references
            set testimonial = :testimonial, image = :image, person= :person, position= :position where id = :id;";

        $result = $pdo->prepare($sql);
        $result->bindParam(':id', $_POST['Id']);
        $result->bindParam(':testimonial', $_POST['Testimonial']);
        $result->bindParam(':image', $_POST['Image']);
        $result->bindParam(':person', $_POST['Person']);
        $result->bindParam(':position', $_POST['Position']);
        $result->execute();
        header("Location: /admin/references/");
    }
}
else {
    require_once ('../db.php');
    $sql = "select * from tb_references where id = :id;";

    $result = $pdo->prepare($sql);
    $result->bindParam(':id', $_GET['id']);
    $result->execute();
    $data = $result->fetch(PDO::FETCH_ASSOC);
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>References</title>
</head>
<body>
<br>
<a href="index.php"> Back </a>
<br><br>
<form action="" method="POST">
    <input type="hidden" name="Id" value="<?=$data['id'] ?? $_GET['id']?>">
    <label for="testimonial">Testimonial: </label><input type="text" name="Testimonial" id="testimonial" value="<?=$data['testimonial'] ?? $_POST['Testimonial']?>">
    <?php if (count($errorBag['testimonial']) > 0):?>
        <?php foreach ($errorBag['testimonial'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="image">Image: </label><input type="text" name="Image" id="image" value="<?=$data['image'] ?? $_POST['Image']?>">
    <?php if (count($errorBag['image']) > 0):?>
        <?php foreach ($errorBag['image'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="person">Person: </label><input type="text" name="Person" id="company" value="<?=$data['person'] ?? $_POST['Person']?>">
    <?php if (count($errorBag['person']) > 0):?>
        <?php foreach ($errorBag['person'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="position">Position: </label><input type="text" name="Position" id="position" value="<?=$data['position'] ?? $_POST['Position']?>">
    <?php if (count($errorBag['position']) > 0):?>
        <?php foreach ($errorBag['position'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <input type="submit" value="Update">
</form>
</body>
</html>
