<?php
require_once ('../db.php');
$sql = "select * from professional_skills;";

/** @var PDO $pdo */
$result = $pdo->prepare($sql);
$result->execute();

$data = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Professional skills</title>
</head>
<body>
    <br><a href="create.php">Add</a> | <a href="/admin/">Back</a><br><br>
    <table border="1">
        <th>ID</th>
        <th>Name</th>
        <th>Score</th>
        <th>Actions</th>
        <?php foreach ($data as $item): ?>
            <tr>
                <td><?=$item['id']?></td>
                <td><?=$item['name']?></td>
                <td><?=$item['score']?></td>
                <td><a href="update.php?id=<?=$item['id']?>">edit</a> | <a href="delete.php?id=<?=$item['id']?>">delete</a></td>
            </tr>
        <?php endforeach ?>
    </table>

</body>
</html>
