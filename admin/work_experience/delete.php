<?php
require_once ('../db.php');
/** @var PDO $pdo */
$sql = "delete from work_experience where id = :id;";

$result = $pdo->prepare($sql);
$result->bindParam(':id', $_GET['id']);
$result->execute();

header("Location: /admin/work_experience/");
