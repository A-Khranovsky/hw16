<?php
/** @var PDO $pdo */
$errorBag = [
    'start_date' => [],
    'end_date' => [],
    'company' => [],
    'position' => [],
    'description' => []
];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $start_date = $_POST['Start_date'];
    $end_date = $_POST['End_date'];
    $company = $_POST['Company'];
    $position = $_POST['Position'];
    $description = $_POST['Description'];

    if (empty($start_date)) {
        $errorBag['start_date'][] = 'Поле не должно быть пустым';
    } else {
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $start_date)) {
            $errorBag['start_date'][] = 'Формат даты должен быть YYYY-MM-DD';
        } else {
            $buff_start_date = new DateTime($start_date);
        }
    }

    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $end_date)) {
        $errorBag['end_date'][] = 'Формат даты должен быть YYYY-MM-DD';
    } else {
        $buff_end_date = new DateTime($end_date);
    }
    if ($buff_end_date < $buff_start_date) {
        $errorBag['end_date'][] = 'Конечная дата не может быть меньше начальной';
    }
    if ($buff_end_date == $buff_start_date) {
        $errorBag['start_date'][] = 'Начальна дата не должна быть равна конечной';
        $errorBag['end_date'][] = 'Конечная дата не может быть равна начальной';
    }

    if (empty($company)) {
        $errorBag['company'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($company)) {
            $errorBag['company'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($company) < 5) {
                $errorBag['company'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($company) > 255) {
                $errorBag['company'][] = 'Значение более 255 символов';
            }
        }
    }

    if (empty($position)) {
        $errorBag['position'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($position)) {
            $errorBag['position'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($position) < 5) {
                $errorBag['position'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($position) > 255) {
                $errorBag['position'][] = 'Значение более 255 символов';
            }
        }
    }

    if (empty($description)) {
        $errorBag['description'][] = 'Поле не должно быть пустым';
    } else {
        if (is_numeric($description)) {
            $errorBag['description'][] = 'Значение не должно быть числом';
        } else {
            if (mb_strlen($description) < 5) {
                $errorBag['description'][] = 'Значене менее 5 символов';
            }
            if (mb_strlen($description) > 255) {
                $errorBag['description'][] = 'Значение более 255 символов';
            }
        }
    }
    $errorsCounter = count($errorBag['start_date'] + $errorBag['end_date'] + $errorBag['company'] + $errorBag['position'] + $errorBag['description']);
    if ($errorsCounter == 0) {
        require_once('../db.php');
        $sql = "INSERT INTO work_experience (start_date, end_date, company, position, description) VALUES (:start_date, :end_date, :company, :position, :description);";
        $result = $pdo->prepare($sql);

        $result->bindParam(':start_date', $_POST['Start_date']);
        $result->bindParam(':end_date', $_POST['End_date']);
        $result->bindParam(':company', $_POST['Company']);
        $result->bindParam(':position', $_POST['Position']);
        $result->bindParam(':description', $_POST['Description']);
        $result->execute();

        header("Location: /admin/work_experience/");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Work experience</title>
</head>
<body>
<br>
<a href="index.php"> Back </a>
<br><br>
<form action="" method="POST">
    <label for="start_date">Start date: </label><input type="text" name="Start_date" id="start_date" value="<?=$_POST['Start_date'] ?? ''?>">
    <?php if (count($errorBag['start_date']) > 0):?>
        <?php foreach ($errorBag['start_date'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="end_date">End date: </label><input type="text" name="End_date" id="end_date" value="<?=$_POST['End_date'] ?? ''?>">
    <?php if (count($errorBag['end_date']) > 0):?>
        <?php foreach ($errorBag['end_date'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="company">Company: </label><input type="text" name="Company" id="company" value="<?=$_POST['Company'] ?? ''?>">
    <?php if (count($errorBag['company']) > 0):?>
        <?php foreach ($errorBag['company'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="position">Position: </label><input type="text" name="Position" id="position" value="<?=$_POST['Position'] ?? ''?>">
    <?php if (count($errorBag['position']) > 0):?>
        <?php foreach ($errorBag['position'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <label for="description">Description: </label><input type="text" name="Description" id="Description" value="<?=$_POST['Description'] ?? ''?>">
    <?php if (count($errorBag['description']) > 0):?>
        <?php foreach ($errorBag['description'] as $error):?>
            <p> <?=$error ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <br><br>
    <input type="submit" value="Create">
</form>
</body>
</html>